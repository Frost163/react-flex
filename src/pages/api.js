import React, { Component } from 'react';
import {Link, NavLink} from 'react-router-dom';


class Api extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false

        };
    }
    componentDidMount()
    {
        fetch('http://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded: true,
                    items: json

                })
            });
    }


    render() {
        var {isLoaded, items } = this.state;
        if (isLoaded )
        {
            return (
                <div>
                    <ul>
                        {items.map  ( item => (
                            <li key = {item.id}>
                                Name: {item.name}
                            </li>
                        ))};
                    </ul>
                </div>);

        }

export default Api:


        <div>
            <NavLink to="/api" activeClassName="PageSwitcher__Item--Active"
                     className="PageSwitcher__Item">Api</NavLink>
            <ul>
                {items.map  ( item => (
                    <li key = {item.id}>
                        Name: {item.name}
                    </li>
                ))};
            </ul>
        </div>

        <img src="https://media1.tenor.com/images/6c694726c00d5b526790878676273aab/tenor.gif?itemid=13369215" alt="альтернативный текст">
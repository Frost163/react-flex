import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            login: '',
            email: '',
            password: '',
            surname: '',
            name: '',
            age: '',
            formErrors: {email: '', password: '', login: '', surname: '', name:'', age:''},
            emailValid: false,
            passwordValid: false,
            formValid: false,
            loginValid: false,
            surnameValid: false,
            nameValid: false,
            ageValid: false

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        var {login, surname, name, password, email, age } = this.state;
        var event = {
            login: login,
            surname: surname,
            name: name,
            password: password,
            email: email,
            age: age
        };

        if (!login || !email || !surname || !name || !password || !age  || age<0 || age > 100   )
        {
            alert ('Error, check!');
        }
else {
            if (localStorage.getItem(login ) == null ) {
                var str = JSON.stringify(event);
                event = JSON.parse(str);
                alert ('Successful');
                alert(str);
                localStorage.setItem(login , login);
                localStorage.setItem(login+password , login+password);


            } else {
                alert('login already exists')
            }
        }




        console.log('The form was submitted with the following data:');



        console.log(this.state);

    }




    render() {
        return (

            <div className="FormCenter">

                <form onSubmit={this.handleSubmit} className="FormFields">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="login">Login</label>
                        <input type="text" id="login" className="FormField__Input" placeholder="Enter your login" name="login" value={this.state.login} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="surname">Surname</label>
                        <input type="text" id="surname" className="FormField__Input" placeholder="Enter your surname" name="surname" value={this.state.surname} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="name">Name</label>
                        <input type="text" id="name" className="FormField__Input" placeholder="Enter your name" name="name" value={this.state.name} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="password">Password</label>
                        <input type="password" id="password" className="FormField__Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="email">E-Mail Address</label>
                        <input type="email" id="email" className="FormField__Input" placeholder="Enter your email" name="email" value={this.state.email} onChange={this.handleChange} />
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="age">Age</label>
                        <input type="int" id="age" className="FormField__Input" placeholder="Enter your age" name="age" value={this.state.age} onChange={this.handleChange} />
                    </div>


                    <div className="FormField">
                        <button className="FormField__Button mr-20">Sign Up</button> <Link to="/sign-in" className="FormField__Link">I'm already member</Link>
                    </div>
                </form>
            </div>
        );
    }
}

export default SignUpForm;

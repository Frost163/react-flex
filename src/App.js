import React, { Component } from 'react';
import { HashRouter as Router, Route, Link, NavLink } from 'react-router-dom';
import './App.css';
import SignUpForm from './pages/SignUpForm';
import SignInForm from './pages/SignInForm';
import jsonData from "./pages/data.json";
console.log(jsonData);





class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            isLoaded: false

        };
    }
        componentDidMount()
        {
            fetch('http://jsonplaceholder.typicode.com/users')
                .then(res => res.json())
                .then(json => {
                    this.setState({
                        isLoaded: true,
                        items: json

                    })
                });
        }


    render() {


var {isLoaded, items } = this.state;




if (!isLoaded )
{
    return (

<div>

        <ul>
            {items.map  ( item => (
                <li key = {item.Age}>
                     {item.Age}


                </li>
            ))};
        </ul>

</div>);


}

    return (

        <Router basename="/react-auth-ui/">

            <div className="App">


                    <div className="App__Aside"></div>

                <div className="App__Form">
                    <div className="PageSwitcher">
                        <NavLink to="/sign-in" activeClassName="PageSwitcher__Item--Active"
                                 className="PageSwitcher__Item">Sign In</NavLink>
                        <NavLink exact to="/" activeClassName="PageSwitcher__Item--Active"
                                 className="PageSwitcher__Item">Sign Up</NavLink>



                    </div>

                    <div className="FormTitle">
                        <NavLink to="/sign-in" activeClassName="FormTitle__Link--Active" className="FormTitle__Link">Sign
                            In</NavLink> or <NavLink exact to="/" activeClassName="FormTitle__Link--Active"
                                                     className="FormTitle__Link">Sign Up</NavLink>
                    </div>

                    <Route exact path="/" component={SignUpForm}>
                    </Route>
                    <Route path="/sign-in" component={SignInForm}>
                    </Route>

                </div>

            </div>
        </Router>


    );
}

}

export default App;
